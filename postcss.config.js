// TODO: Configure purge script for production.
// const purgecss = require('@fullhuman/postcss-purgecss')({
//   content: ['./components/**/*.twig', './templates/**.*.html.twig'],
//   defaultExtractor: content => content.match(/[A-Za-z0-9-_:/]+/g) || []
// })

module.exports = {
  plugins: [
    require('postcss-import'),
    require('tailwindcss'),
    require('postcss-nested'),
    require('postcss-custom-properties'),
    require('autoprefixer'),
    require('cssnano')
  ]
}
