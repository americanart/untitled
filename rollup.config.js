import copy from 'rollup-plugin-copy';
import commonjs from '@rollup/plugin-commonjs';
import cleaner from 'rollup-plugin-cleaner';
import resolve from '@rollup/plugin-node-resolve';
import { terser } from 'rollup-plugin-terser';

export default {
  input: 'src/js/ui.js',
  output: [
      {
        file: 'dist/js/ui.js',
        format: 'iife',
        sourcemap: true
      },
      {
        file: 'public/js/ui.js',
        format: 'iife',
        sourcemap: true
      }
  ],
  plugins: [
    resolve(), // tells Rollup how to find date-fns in node_modules
    commonjs(), // converts date-fns to ES modules
    terser(), // Minify code.
    copy({
      targets: [
        { src: './node_modules/superagent/dist/superagent.min.js', dest: ['./dist/js', './public/js']},
        { src: './src/components/*', dest: './dist/components' },
      ]
    }),
    cleaner({
      targets: [ // Delete existing assets before build.
        './dist/js',
        './dist/components',
        './public/js'
      ]
    })
  ]
};
