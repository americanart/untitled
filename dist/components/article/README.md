# Article

A complete view of a topic, object, or information. An article includes common
elements like a Title and Subtitle, and may include other elements and patterns.

## TODO: 
- Add an "author" block
- layout containers?

## Variables
- `id`: The element "id" attribute
- `title`: The Article title
- `subtitle`: The Article subtitle
- `classes`: An array of classes to override the default component classes
- `extra_classes`: An array of classes to append to the default component classes

## Example Usages

## Accessibility

## Changelog
