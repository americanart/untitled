# Spinner

A common loading icon with configurable sizes and stroke colors.

## Variables

- `id`: The element "id" attribute
- `size`: (_xsmall_/_small_/_medium_/_large_/_xlarge_) The spinner size (default: medium)
- `modifier`: The component variant
- `classes`: An array of classes to override the default component classes
- `foreground`: (_light_/_dark_) The fill color of the svg should be light or dark

## Variants

- `primary`
- `secondary`
- `tertiary`

## Example Usage

```twig
{% include "@components/spinner/spinner.twig" with {
    "size": "large"
} only %}
```

## Accessibility

- role: alert
- aria-live: assertive

