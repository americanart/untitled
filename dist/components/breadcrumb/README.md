# Breadcrumb

## Variables

- `id`: The element "id" attribute
- `items`: The nav items
- `classes`: An array of classes to override the default component classes


## Example Usage

```twig
{% include "@components/breadcrumb/breadcrumb.twig" with {
    id: "example-crumb",
    items: [
      {
        "href": "/",
        "text": "Home"
      },
      {
        "href": "/about",
        "text": "About Us"
      }
    ]
} only %}
```

## Accessibility
- role: navigation

## Changelog
