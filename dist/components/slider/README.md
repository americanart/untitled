# Slider

A flexible carousel slide component. A slider is most often used with images, but
can also be used to slide between video, text, or other content, and can be combined
with other components to create different types of UI patterns.

## TODO
- handle different types of content as slider item.

## Variables

- `id`: The slider container id attribute
- `classes`: An array of classes to override the default component classes
- `items`: The slider nav items
- `size`: (_xsmall_/_small_/_medium_/_large_/_xlarge_) The slider relative size (default: medium)

## Example Usages

## Accessibility

## Changelog
