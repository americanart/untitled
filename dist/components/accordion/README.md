# Accordion

A simple navigation element to organize grouped panels of content.

## TODO
- Accessibility: https://www.w3.org/TR/wai-aria-practices-1.1/examples/accordion/accordion.html
- also see, https://www.si.edu/faqs for accessibility example
- How to handle customizing classes/colors of item headers?

## Variables

- `id`: The element "id" attribute
- `classes`: An array of classes to override the default component classes
- `extra_classes`: An array of classes to append to the default component classes
- `items` Array of accordion item objects
    - `header` The item header
    - `content` The item's full content
- `collapsible`: (_true_/_false_) Are accordian items collapsible (default: false)
- `selected`: The numeric number of the item to be "expanded" by default. For example, `"selected": 1` would render with the first accordion item expanded (default: none)
## Example Usages

```twig
{% include "@components/accordion/accordion.twig with { 
    id: "faq",
    collapsible: true,
    selected: 1,
    items: [
      {
        header:
          "What are your admission fees and hours?",
        content:
        "<p>Admission is <strong>free</strong> for all Smithsonian museums and the zoo in Washington, D.C., and the American Indian Museum's George Gustav Heye Center in New York. Tickets are not used for general admission with the exception of the African American History and Culture Museum; visit the museum's website for details.</p><p>A fee is required at the Cooper Hewitt, Smithsonian Design Museum in New York (Members and children under age 12 are admitted free);</p><p>Washington, D.C. museums are open every day except December 25.</p>"
      },
      {
        header:
          "Can the Smithsonian provide appraisals?",
        content:
        "<p>Smithsonian staff is unable to comment on (e.g., speculate on value, intended use, authenticity, age, or materials) or to do research on items outside our own collections. Smithsonian policy prevents us from recommending a specific appraiser or dealer. You can ascertain the current range of prices for items exchanged at sales and auctions by consulting the price guides available in bookstores, public libraries, or online. Also, dealers actively engaged in buying and selling antiques, professional appraisers attached to sales galleries, and official appraisers for local probate courts usually can give prevailing values. Information on providers of these services can be found in collectors' magazines, in the yellow pages of telephone directories, or in other sources available from the reference librarian in your area library. Also see general information on object appraisals.</p>"
      }
    ]
  }
} only %}
```
## Accessibility

## Changelog
