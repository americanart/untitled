# Digest

A Digest component is different than a Card because not only media alignment, but difference
in purpose. A card is a jumping off to other content and might not necessarily include "article"
content while a Digest is an abbreviated summary of Article content.

## TODO:
- replace header, footer, body with blocks.
- make "compact" compact
- clean up styles
- sub components

## Variants

- `primary`
- `secondary`
- `tertiary`
- `compact`
- `media-right`

## Variables

- `id`: The section element id attribute
- `modifier`: The component variant
- `title`: The digest title/header (is in the "header" block)
- `href`: The digest link
- `body`: Digest body text/content (is in the "content" block)
- `classes`: An array of classes to override the default component classes

- `foreground`: (_light_/_dark_) The brightness of foreground elements (text, icons, etc.)
- `image`: An image object (see @components/image)
    - `sizes`: An array of image src uri's by size (_default_/_small_/_medium_/_large_/_xlarge_)
    - `width`: The image width
    - `height`: The image height
    - `alt`: Alt text
    - `orientation` The image orientation (_protrait_/_landscape_/_square_)
    - `label`: The WAI-ARIA description for the image
- `badge`: A badge object (see @components/componenents/badge)
    - `id`: The element "id" attribute
    - `modifier`: The component variant name
    - `text`: The badge text
    - `href`: The badge link

## Blocks

- `header`: The digest header block
- `content`: The digest main content block
- `footer`: The digest footer block

## Example Usage

## Accessibility

## Changelog
