# Dropdown

A dropdown navigation element.

## Variables

- `id`: The element "id" attribute
- `text`: Text for the parent item
- `href`: The (optional) url for parent item
- `event`: (_hover_/_click_) The event type to trigger the dropdown
- `classes`: An array of classes to override the default component classes

## Example Usages

```twig
    {% embed "@components/dropdown/dropdown.twig" with {
      id: "example-dropdown",
      text: "Spaceships",
      event: "click"
    } %}
      {% block dropdown_content %}
        <ul>
            <li>Battlestar Galactica</li>
            <li>USS Enterprise</li>
            <li>Millennium Falcon</li>
        </ul>
      {% endblock %}
    {% endembed %}
```
## Accessibility

## Changelog
