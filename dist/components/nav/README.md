# Nav

A basic navigation menu.

## Variables

- `id`: The element "id" attribute
- `items`: The nav items
- `classes`: An array of classes to override the default component classes


## Example Usages

```twig
{% include "@components/nav/nav.twig" with {
    id: "demo-nav",
    items: [
      {
        title: "Visit",
        url: "#",
        items: [
          {
            text: "Visit SAAM",
            active: true,
            href: "#",
            items: [
              {
                text: "Luce Foundation Center",
                href: "#"
              },
              {
                text: "Lunder Conservation Center",
                href: "#"
              }
            ]
          },
          {
            text: "Visit Renwick",
            href: "#"
          }
        ]
      }
    ]
} only %}


```
## Accessibility

## Changelog
