# Text

A common text field form element.


## Variables

- `type`: _text_/_email_/_tel_ The HTML5 input type.
- `id`: The element "id" attribute
- `label`: The form label
- `label_visible`: _true (default)_/_false_ Show label or screen reader only.
- `name`: The form field name
- `help`: Additional help text.
- `classes`: An array of classes to override the default component classes
- `size`: The field size.
- `maxlength`: Max length of the input value.
- `placeholder`: Placeholder text to display in the field.
- `required`: _true_/_false_ This is a required field.
- `pattern`: A regular expression to check against input value.
- `x_attributes`: A key'ed array of "x" attributes to add interactivity.

## Example Usages

## Accessibility

