# Checkbox

A single form checkbox.


## Variables

- `id`: The element "id" attribute
- `label`: The form label
- `label_visible`: _true (default)_/_false_ Show label or screen reader only.
- `name`: The form field name
- `value`: The field value.
- `help`: Additional help text.
- `classes`: An array of classes to override the default component classes
- `required`: _true_/_false_ This is a required field.
- `checked`: _true_/_false (default)_ The checkbox is checked.
- `x_attributes`: A key'ed array of "x" attributes to add interactivity.

## Example Usages

## Accessibility

