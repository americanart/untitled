# Textarea

A text area form element.


## Variables

- `id`: The element "id" attribute
- `label`: The form label
- `name`: The form field name
- `help`: Additional help text.
- `classes`: An array of classes to override the default component classes
- `rows`: The number of textarea rows. (Default: 9)
- `columns`: The number of textarea columns. (Default: 3)
- `required`: _true_/_false_ This is a required field.

## Example Usages

## Accessibility

