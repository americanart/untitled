# Form

## Variables

- `id`: The element id attribute.
- `classes`: An array of classes to override the default component classes.
- `action`: The form action.
- `method`: _GET_/_POST_ The submit method.
- `token`: The CSRF token to protect agains Cross Site Request Forgery.
- `x_attributes`: A key'ed array of "x" attributes to add interactivity. (Note: `x-ref` and `x-data` are set by default)

## Blocks

- `content`: The form body.
- `feedback`: Container for feedback/error messages.

------------

### Form Fields

#### [Checkbox](./checkbox/README.md)

A single checkbox input field.

#### [Datepicker](../datepicker/README.md)

A input field with date picker.

#### [Text](./checkboxes/README.md)

A group of checkboxes.

#### [Radios](./radios/README.md)

A group of radio buttons.

#### [Text](./text/README.md)

A common text input form field.

#### [Textarea](./textarea/README.md)

A text area form field.

#### [Select](./select/README.md)

A select form field.
