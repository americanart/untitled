# Alert

## Variants

- `primary`
- `success`
- `warning`
- `danger`
- `ghost`

## Variables

- `id`: The element "id" attribute
- `modifier`: The component variant name
- `title`: A heading title 
- `messages`: A list of messages
- `classes`: An array of classes to override the default component classes
- `extra_classes`: An array of classes to append to the default component classes
- `foreground`: (_light_/_dark_) The brightness of foreground elements (text, icons, etc.)
- `dismissible`: (_true_/_false_) The alert can be closed/dismissed.

## Example Usages

An alert with modifier:

```twig
{% include "@components/alert/alert.twig" with {
    id: "example-alert",
    modifier: "danger",
    title: "Important message!",
    messages: [
      'The Smithsonian American Art Museum will be closed for an event.'
    ]
} only %}
```

## Accessibility
- role: alert


