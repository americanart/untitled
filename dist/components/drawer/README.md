# Drawer

A "off canvas" navigation element.
Note: The drawer component depends on a "relative" parent container.

## Variables

- `id`: The element "id" attribute
- `title`: Text for the parent item
- `classes`: An array of classes to override the default component classes

## Blocks

- `content`: The drawer main content

## Example Usages

```twig
{% embed "@components/dropdown/dropdown.twig" with { id: "demo-drawer" } only %}
    {% block content %}
        <p>This is some content...</p>
    {% endblock %}
{% endembed %}
```
## Accessibility

## Changelog
