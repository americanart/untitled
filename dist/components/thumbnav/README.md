# Thumbnav

A thumbnail image navigation element.

## TODO
- a variant to output images in their actual dimensions (remove object-cover)
- overflow hidden and next/previous buttons
- needs to be able to specify height of nav (so it could be used as a slider)

## Variables

- `id`: The element id attribute
- `classes`: An array of classes to override the default component classes

- `images`: An array of image objects (see @components/image)

## Example Usages

## Accessibility

## Changelog
