---
title: "{{ Untitled }} Design System"
description: "Design System for the Smithsonian American Art Museum"
---

*Untitled* is a Design System and pattern library for the [Smithsonian American Art Museum](https://americanart.si.edu).
It serves as a resource that helps to define and implement a common visual language for our web applications.

## Links
- [Project Source Code](https://gitlab.com/americanart/untitled)
- [Smithsonian Institution Brand Guidelines](https://logo.si.edu/)


