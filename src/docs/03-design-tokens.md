---
title: Design Tokens
description: ""
---

Tokens are the atoms of the system and are used instead of hard coded values to ensure consistency.

## Color Palette
TODO

## Headings
TODO

## Typography

- regular and special paragraph styles (like "intro" or "lead")
