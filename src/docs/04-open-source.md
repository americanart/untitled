---
title: Open Source Credit
---

## We ❤️ Open Source software!

This project uses the following open source projects (among the many we use each day).

Huge thanks to all contributors!:

### [Fractal](https://fractal.build)
A tool to help you build & document web component libraries, and then integrate them into your web projects.
- [Contributors](https://github.com/frctl/fractal/graphs/contributors)
- License: [MIT](https://github.com/frctl/fractal/blob/develop/LICENSE)

### [TailwindCSS](https://tailwindcss.com/)
A utility-first CSS framework for rapidly building custom designs.
- [Adam Wathan](https://twitter.com/adamwathan) and [contributors](https://github.com/tailwindcss/tailwindcss/graphs/contributors)
- License: [MIT](https://github.com/tailwindcss/tailwindcss/blob/master/LICENSE)

### [AlpineJS](https://github.com/alpinejs/alpine)
A rugged, minimal framework for composing JavaScript behavior in your markup.
- [Caleb Porzio](https://twitter.com/calebporzio) and [contributors](https://github.com/alpinejs/alpine/graphs/contributors).
- License: [MIT](https://github.com/alpinejs/alpine/blob/master/LICENSE.md)
