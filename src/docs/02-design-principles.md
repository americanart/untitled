---
title: Design Principles
---

These principles are the foundation of Castle UI. They are here to guide our team in making better and more informed design decisions.

## 1. Create compelling experiences.

The Smithsonian lists several [Grand Challenges](https://www.si.edu/about/mission) for the organization, including "Understanding the American Experience", "Magnifying the Transformative Power of Arts and Design", and "Unlocking the Mysteries of the Universe". Wow! These are monumental goals that won't be achieved without taking risks. We must strive to do more than deliver canned content to our audience. Our digital products must **tell stories**, **engage**, **inspire**, and **delight** our users.

## 2. Be consistent.

Consistently applying similar patterns to similar problems is one of the most effective ways to make our users feel comfortable using our products. Similar to how we assist our users finding their way through our museums, we apply consistent design patterns to make our digital products as **intuitive** and **friendly** as possible. We want to be inviting, and therefore our users need to not only feel at home using our digital products, but be able to walk around with the lights off.

## 3. Be inclusive

Millions of people visit the Smithsonian each year, and we design our digital products with **all** of them in mind, including people with different physical, mental health, social, cultural or learning needs. With each new iteration, we remind ourselves that the word "accessibility", broken down, means the **ability to access**, and evaluate if we've provide that access to everyone.

## 4. Be clear (and calm)

The Smithsonian has been called "America's Attic". That's good for describing the enormous amount of content we have, but the exact opposite of what we want our digital products to be. We want to provide visual clarity in all our products. **Clear** and **simple** interfaces, promoting principles of [calm technology](https://calmtech.com/), will best help our audience navigate the "attic". 

## 5. Open up

Create in an **open** and **transparent** manner. Engagement means listening, and being honest about what we are doing, why we are doing it, and it's efficacy. Embrace (and support) [Open Source Software](/docs/open-source.html). Share, collaborate, learn, and iterate together.

## 6. ONE Smithsonian

Metaphors are often used in designing for digital products to make a digital experience familiar by emulating a real world one. For us, the metaphor is obvious, using our digital products is like visiting one of our museums. (Okay, maybe that's a simile) We should design, guided by these stated principles, to create unified digital experiences that consistently represent the organization. Likewise, we follow a [family branding](https://en.wikipedia.org/wiki/Umbrella_brand) strategy that ties our digital products to the Smithsonian, the world’s largest museum, education, and research complex.



