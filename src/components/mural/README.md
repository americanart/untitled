# Mural

A large display section with a background graphic. Often used as a section/page heading or call-to-action.

## TODO:

- create a variant where the component expands to the full height of the viewport
- Add an overlay with opacity which can be used to make overlaid text more legible (see: https://boltdesignsystem.com/pattern-lab/?p=viewall-components-background)

## Variables

- `title`: Title
- `subtitle`: Subtitle
- `image`: An image object (see @components/image)
- `classes`: An array of classes to override the default component classes
- `foreground`: (_light_/_dark_) The brightness of foreground elements (text, icons, etc.)
- `button`: A button object (see @components/button)

## Blocks

- `content`: The center content

## Example Usages

Standard include:

```twig

```

Using embed to override the "content" block

```twig

```

## Accessibility

## Changelog
