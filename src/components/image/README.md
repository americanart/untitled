# Image

An image element with srcset attributes.

## Variants

- `round`

## Variables

- `id`: The section element id attribute
- `sizes`: An object of src uri's keyed by size. Keys: (_default_/_small_/_medium_/_large_/_xlarge_)
- `alt`: The alt text
- `width`: The optional image width
- `height`: The optional image height
- `orientation` The image orientation (_protrait_/_landscape_/_square_)
- `label`: The WAI-ARIA description
- `title`: The image title attribute
- `caption`: The image caption
- `href`: The image link
- `classes`: An array of classes to override the default component classes

## Example Usages

## Accessibility

## Changelog
