# SVG

A Scalar Vector Graphics element

## Variables

- `id`: The element id attribute
- `sizes`: An object of src uri's keyed by size. Keys: (_default_/_small_/_medium_/_large_/_xlarge_)
- `width`: The element width
- `height`: The element height
- `alt`: The alt tag attribute
- `itemscope`: Microdata itemscope attribute
- `itemtype`: Microdata itemtype attribute
- `itemprop`: Microdata itemtype attribute

## Example Usages

## Accessibility

## Changelog
