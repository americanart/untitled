# Radios

Radio buttons form element.

## Variants

- `inline`

## Variables

- `id`: The container element "id" attribute
- `label`: The form label
- `name`: The form field name
- `classes`: An array of classes to override the default component classes
- `items`: A key/value array of radios.
- `selected`: The option key as default selection.
- `required`: _true_/_false_ This is a required field.
- `x_attributes`: A key'ed array of "x" attributes to add interactivity.

## Example Usages

## Accessibility

