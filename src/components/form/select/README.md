# Select

A select form element.


## Variables

- `id`: The element "id" attribute
- `label`: The form label
- `name`: The form field name
- `classes`: An array of classes to override the default component classes
- `options`: A key/value array of select options.
- `selected`: The option key as default selection.
- `required`: _true_/_false_ This is a required field.

## Example Usages

## Accessibility

