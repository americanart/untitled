# Checkboxes

A group of checkboxes with the same name attribute.


## Variables

- `id`: The element "id" attribute of the container element.
- `legend`: The group label
- `legend_visible`: _true (default)_/_false_ Show label or screen reader only.
- `name`: The form field name
- `classes`: An array of classes to override the default component classes
- `items`: An array of checkboxes. (see @components/form/checkbox)

## Example Usages

## Accessibility

