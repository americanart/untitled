# Modal

## Variants

- `fullscreen`

## Variables

- `id`: The element "id" attribute
- `title`: The modal title/header (is in the "header" block)
- `body`: The modal body text/content (is in the "content" block)
- `open`: (_true_/_false_) Model open by default
- `classes`: An array of classes to override the default component classes

## Blocks

- `header`: The modal header
- `content`: The modal main content
- `footer`: The modal footer

## Example Usages

```twig
{% embed "@components/modal/modal.twig" with {
  "id":"modal-example",
  "open": "false",
  "title":"This is a fullscreen modal",
  "modifier": "fullscreen"
} %}
  {% block content %}
    <p>Say hello to my little Modal!.</p>
  {% endblock %}
{% endembed %}

```
## Accessibility

## Changelog
