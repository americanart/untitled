# Tip

A discrete popover/tooltip element used to provide a user with some context information or indication
that some action was taken

## Variables

- `id`: The element id attribute
- `text`: The tip text
- `position`: (_top_/_bottom_/_right_/_left_) The position to orientate the tip to parent element (default: top)
- `classes`: An array of classes to override the default component classes
- `x_show`: A simple javascript expression, used in conjunction with other UI components to control visibility. For example: "visbile = true" would output the attribute `x-show="visible=true"`

## Example Usages

## Accessibility

## Changelog
