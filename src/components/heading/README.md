# Heading

A common the heading or title for a page, section, or component.

# Variables

- `element`: (_h1_/_h2_/_h3_/_h4_/_h5_/_h6_/_p_/_span_/_cite_/_div_) The containing HTML element (default: "div")
- `id`: The heading id attribute
- `text`: The heading text
- `size`: (_xsmall_/_small_/_medium_/_large_/_xlarge_) The heading font size (default: large)
- `weight`: (_light_/_normal_/_semibold_/_bold_/extrabold) The font weight (default: semibold)
- `classes`: An array of classes to override the default component classes
- `extra_classes`: An array of classes to append to the default component classes
- `foreground`: (_light_/_dark_) The brightness of foreground text

## Example Usages

```twig
{% include "@components/heading/heading.twig" with {
    "text": "Welcome to the Smithsonian American Art Museum",
    "element": "h2",
} only %}
```

## Accessibility

## Changelog
