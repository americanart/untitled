# Button

## TODO:
- Move the text colors to a "foreground" variable

# Variables

- `element`: The containing HTML element (default: "button")
- `id`: The section element id attribute
- `modifier`: The component variant
- `text`: The button text
- `href`: The button link
- `classes`: An array of classes to override the default component classes
- `foreground`: (_light_/_dark_) The brightness of foreground elements (text, icons, etc.)
- `size`: (_xsmall_/_small_/_medium_/_large_/_xlarge_) The button width and text size (default: medium)
- `x_attributes`: A key'ed array of x-attributes to add interactivity.

## Variants

- `primary`
- `secondary`
- `tertiary`
- `success`
- `warning`
- `danger`
- `ghost`

## Example Usage

```twig
{% include "@components/button/button.twig" with {
    id: "example-button",
    modifier: "secondary",
    text: "Click me",
} only %}
```

## Accessibility
- role: navigation

## Changelog
