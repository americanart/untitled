# Badge

## Variables

- `id`: The element "id" attribute
- `modifier`: The component variant name
- `text`: The badge text 
- `href`: The badge link
- `classes`: An array of classes to override the default component classes
- `extra_classes`: An array of classes to append to the default component classes

## Example Usages

An alert with modifier:

```twig
{% include "@components/badge/badge.twig" with {
    id: "example-badge",
    modifier: "primary",
    text: "New!"
} only %}
```



