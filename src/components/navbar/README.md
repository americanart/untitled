# Navbar

## TODO
- make navbar left/right a block

## Variants

- `compact`

## Blocks

- `navbar_right`
- `navbar_left`

## Variables

- `id`: The section element id attribute
- `modifier`: The component variant
- `classes`: An array of classes to override the default component classes

- `foreground`: (_light_/_dark_) The brightness of foreground elements (text, icons, etc.)
- `fixed`: (_true_/_false_) The element will position itself fixed on scroll. (default: false)
- `logo`: A logo object
    - `sizes`: An object of src uri's keyed by size. Keys: (_default_/_small_/_medium_/_large_/_xlarge_)
    - `width`: The graphic width
    - `height`: The graphic height
    - `graphic`: The graphic type (_raster_/_vector_)
    - `alt`: Alt text
    - `itemscope`: The scope of related metadata
    - `itemtype`: The schema.org vocabulary
- `items`: An array of navigation menu items (see @components/nav)

## Example Usages

```twig
  {% set logo = {
    "sizes": {"default": "/assets/images/logo.svg"},
    "width": "50",
    "alt": "Home",
    "graphic": "vector",
    "itemscope": true,
    "itemtype": "https://schema.org/Organization"
  } %}
  {% set nav = menu.items  %}
  {% include "@components/navbar/navbar.twig" with {
    "id": "main-menu",
    "items": nav,
    "logo": logo
  } only %}

```
## Accessibility

## Changelog
