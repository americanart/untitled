# Tabs

A simple navigation element to organize multiple panels of similar/related content

## TODO:
- need to add a "target" for each tab nav element that will be either a link or a hidden element to show

## Variables

- `id`: The section element id attribute
- `classes`: An array of classes to override the default component classes

- `items`: An array of navigation menu items (see @components/nav)
- `content`: The panel content to be shown/hidden by tabs

## Example Usages

## Accessibility

## Changelog
