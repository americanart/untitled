# Tombstone

A description list element used to output a list of terms and their description/definitions.

## TODO
- a variant to change/remove dividers
- a variant to make inline term: description

## Variables

- `id`: The list id attribute
- `classes`: An array of classes to override the default component classes
- `items`: An array of definition list item objects
    - `term`: The term name
    - `description`: The term description

## Example Usages

## Accessibility

## Changelog
