#Section

A container element with replacement blocks and simple settings to control layout.

## Variables

- `element`: The containing HTML element (default: "section")
- `id`: The section element id attribute
- `modifier`: The component variant
- `classes`: An array of classes to override the default component classes
- `role`: The value of the WAI-ARIA Role attribute
- `foreground`: (_light_/_dark_) The brightness of foreground elements (text, icons, etc.)
- `custom_attributes`: A string of custom attributes

## Variants

- `primary`
- `secondary`
- `tertiary`

## Blocks

- `header`: The section header
- `content`: The section main content
- `footer`: The section footer

## Example Usages
```twig
{% embed "@components/section/section.twig" with {
    "id": "my-example-section",
    "modifier": "primary",
    "foreground": "light",
    "extra_classes": ["dark-section", "call-to-action-section"]
} %}
  {% block header %}
    <h3 class="font-heading font-semibold text-lg">A section about Nam June Paik</h3>
  {% endblock %}
  {% block content %}
    <p>Nam June Paik (1932–2006), internationally recognized as the "Father of Video Art," created a large body of work including video sculptures, installations, performances, videotapes and television productions. He had a global presence and influence, and his innovative art and visionary ideas continue to inspire a new generation of artists.</p><p>Born in 1932 in Seoul, Korea, to a wealthy industrial family, Paik and his family fled Korea in 1950 at the outset of the Korean War, first to Hong Kong, then to Japan. Paik graduated from the University of Tokyo in 1956, and then traveled to Germany to pursue his interest in avant-garde music, composition and performance. There he met John Cage and George Maciunas and became a member of the neo-dada Fluxus movement. In 1963, Paik had his legendary one-artist exhibition at the Galerie Parnass in Wuppertal, Germany, that featured his prepared television sets, which radically altered the look and content of television.</p>
  {% endblock %}
  {% block footer %}
    <footer>More information available at the <em>Smithsonian American Art Museum</em></footer>
  {% endblock %}
{% endembed %}

```

## Accessibility

- role: region

## Changelog
