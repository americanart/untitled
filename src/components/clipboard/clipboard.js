Untitled.clipboard = function() {
  return {
      copied: false,
      copy() {
          let clipboard = this;
          this.$refs.copyMe.focus();
          this.$refs.copyMe.select();
          if (!navigator.clipboard) {
              try {
                  document.execCommand('copy').then(function () {
                      clipboard.copied = true;
                  })
              } catch (err) {
                  console.error('Execute: unable to copy', err);
              }
          } else {
              navigator.clipboard.writeText(this.$refs.copyMe.value).then(function() {
                  clipboard.copied = true;
              }, function(err) {
                  console.error('Async: Could not copy text: ', err);
              });
          }
      },
      isCopied() {
          return this.copied === true
      }
  }
}
