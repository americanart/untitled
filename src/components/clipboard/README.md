# Clipboard

Copy an element's text to the clipboard

## TODO:
- Need to correctly position the tooltip. Note: will this require binding a "relative" class to the parent element?

## Variables

- `id`: The element id attribute.
- `copy_text`: The text to copy.
- `copied_text`: The text to display after copy action.

## Example Usages

## Accessibility

## Changelog
