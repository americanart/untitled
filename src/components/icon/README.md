SVG icons from Zondicons. ([View all icons](https://www.zondicons.com/icons.html))

## TODO:
- add primary, secondary, tertiary variants and adjust color.
- style the optional text next to icon, allow position (top, bottom, left, right)
- handle links
- allow different sizes by adjusting the "viewBox"

## Variables

- `id`: The element "id" attribute
- `icon`: The SVG icon name
- `size`: (_xsmall_/_small_/_medium_/_large_/_xlarge_) The icon size (default: medium)
- `text`: Text to display with the icon
- `href`: Output icon as link
- `button`: (_true_/_false_) Icon is a button
- `classes`: An array of classes to override the default component classes
- `extra_classes`: An array of classes to append to the default component classes
- `foreground`: (_light_/_dark_) The fill color of the icon and text should be light or dark
- `x_attributes`: A key'ed array of "x" attributes to add interactivity. (Note: `x-ref` and `x-data` are set by default)

## Example Usage

```twig
{% include "@componentscomponents/icon/icon.twig" with {
    id: "example-icon",
    icon: "checkmark",
    text: "Checked!",
    href: "#"
} only %}
```

## Accessibility
- role: button

## Changelog
