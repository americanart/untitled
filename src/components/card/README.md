# Card

A card is a common design pattern which presents users as a way of taking action
to discover more information. They commonly exist as links to more detailed
Article content, links, and occasionally as content which supplements a larger subject.

## Variants

- `primary`
- `secondary`
- `tertiary`
- `media-bottom`

## Variables

- `id`: The section element id attribute
- `modifier`: The component variant
- `title`: The card title/header (is in the "header" block)
- `subtitle`: The card subtitle (is in the "header" block)
- `href`: The card link
- `body`: Card body text/content (is in the "content" block)
- `classes`: An array of classes to override the default component classes
- `foreground`: (_light_/_dark_) The brightness of foreground elements (text, icons, etc.)
- `image`: An image object (see [@components/image](../image/README.md))
    - `sizes`: An array of image src uri's by size (_default_/_small_/_medium_/_large_/_xlarge_)
    - `width`: The image width
    - `height`: The image height
    - `alt`: Alt text
    - `orientation` The image orientation (_protrait_/_landscape_/_square_)
    - `label`: The WAI-ARIA description for the image
- `badge`: A badge object (see [@components/badge](../badge/README.md))
    - `id`: The element "id" attribute
    - `modifier`: The component variant name
    - `text`: The badge text
    - `href`: The badge link

## Blocks

- `header`: The card header block
- `content`: The card main content block
- `footer`: The card footer block


## Example Usages

## Accessibility

## Changelog
