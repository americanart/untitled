'use strict';

/* Create a new Fractal instance and export it for use elsewhere if required */
const fractal = module.exports = require('@frctl/fractal').create();
/* Set the title of the project. */
fractal.set('project.title', '{{ Untitled }} Design System');

/*
 * Require the Twig adapter
 */
const twigAdapter = require('@frctl/twig')({
    // if pristine is set to true, bundled filters, functions, tests
    // and tags are not registered.
    // default is false
    pristine: false,
    // if importContext is set to true, all include calls are passed
    // the component's context
    // default is false
    importContext: false,
    // should missing variable/keys emit an error message
    // If false, they default to null.
    // default is false
    strict_variables: false,
    // define Twig namespaces, see https://github.com/twigjs/twig.js/wiki#namespaces
    // this may break some fractal functionality, like including components via their handles and the render tag
    namespaces: {
        'components': '../components'
    },
    // use twig.js default template loader
    // this will allow including templates via relative paths, like twig.js or PHP Twig does by default
    // changing this will break including components via their fractal handles
    // changing this will break the custom render tag
    // default is 'fractal'
    method: 'fractal',
    // register custom functions
    functions: {
        // usage: {{ merge_classes(default_classes, extra_classes, classes) }}
        merge_classes: function(default_classes, append_classes, replacement_classes) {
            // Get a final list of HTML classes given a default list of classes,
            // an optional list of classes to append,
            // and/or a list of classes to replace the default
            if (typeof replacement_classes !== "undefined") {
                if (Array.isArray(replacement_classes)) {
                    return replacement_classes;
                } else {
                    return [replacement_classes];
                }
            }
            if (typeof default_classes !== "undefined" && !Array.isArray(default_classes)) {
                default_classes = [default_classes];
            }
            if (typeof append_classes !== "undefined" && !Array.isArray(append_classes)) {
                append_classes = [append_classes];
            }
            return default_classes.concat(append_classes);
        }
    }
});
/* Set docs engine. (Note: Using Twig this needs to be set before the components engine) */
// fractal.docs.engine(twigAdapter);
/* Set components engine. */
fractal.components.engine(twigAdapter);
fractal.components.set('ext', '.twig');
/* Tell Fractal where the components will live */
fractal.components.set('path', __dirname + '/src/components');
/* Tell Fractal where the documentation pages will live */
fractal.docs.set('path', __dirname + '/src/docs');
/* Specify directory of static assets. */
fractal.web.set('static.path', __dirname + '/public');
/* Set the static HTML build destination. */
fractal.web.set('builder.dest', __dirname + '/build');
/* Set the default preview template. */
fractal.components.set('default.preview', '@preview');
/* Require the Mandelbrot theme module. */
const mandelbrot = require('@frctl/mandelbrot');
/* Create a new instance with custom config options. */
const themeConfig = mandelbrot({
    favicon: "assets/favicon.ico",
    panels: ["notes", "info", "html", "view"],
    skin: "black",
    nav: ["docs", "components"], // show docs above components in the sidebar
    format: "json"
});
/* Use the configured theme by default. */
fractal.web.theme(themeConfig);
/* Use browsersync by default */
fractal.web.set('server.sync', true);
