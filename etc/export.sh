#!/usr/bin/env bash

# Exit immediately on errors.
set -e
# Get the source and destination directories for copy.
DEST=$1
WORKING_DIR="$(dirname "${0}")"
PROJECT_ROOT="$(dirname "${WORKING_DIR}")"
SRC="${PROJECT_ROOT}/dist/*"

# Example: $ ./etc/export.sh ../path/to/copy/dir
if [ $# -eq 0 ]
then
  echo "Missing argument. (Path to destination is required)"
else
  tput bold
  read -r -p "Are you sure you want to copy to ${DEST}? (this will delete any existing files) [Y/n]" response
  tput sgr0
  if [[ "$response" =~ ^(yes|Yes|y|Y)$ ]]
  then
      echo -e "Copying dist to ${DEST} ..."
      rm -rf ${DEST}; mkdir ${DEST} && cp -pr ${SRC} ${DEST}
      ls -hal ${DEST}
  else
      echo "Do nothing."
  fi
fi

echo -e "Finished"
set +v

