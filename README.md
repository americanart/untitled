<div align="center">
  <img src='public/logo.png' width="200" alt="Untitled Design System Logo" style="max-width: 100%;" />
</div>

# Untitled (Design System)

A design system and twig template component library by the [Smithsonian American Art Museum](https://americanart.si.edu)

View [design.saam.media](http://design.saam.media)

## Development

### Install

```
npm install
```

### Start fractal

```
npx fractal start
```

## Project files

- `src/components` contains component library components.
- `src/css` and `src/js` contains the styles and scripts entry points.
- `src/docs` contains the documentation pages.
- `dist` contains the final component library.
- `public` contains the generated static Design System documentation and components explorer.

## Build project

```
npm run build
```

## Twig 

Untitled twig templates use the following custom functions:

- merge_classes

## Copy to a local directory

For convenience, a bash script to copy the component library to a target destination on your local machine

```
./etc/export.sh ../muse3/web/libraries/untitled
``` 

## Deploying the static site

Example deploying to S3 using [s3deploy](https://github.com/bep/s3deploy).

```shell script
s3deploy -bucket=design.saam.media -region=us-east-1 -source=public -public-access
```

Example deploying to S3 with the [AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2.html).

```shell script
aws2 s3 sync ./build s3://design.saam.media --delete --acl public-read
```
